% groups = {'Joint' 'Single' 'Scramble'};% 'Single' 'Scramble'};
% conditions = {'Inter'}; %
% if strcmp(groups{1}, 'Scramble')
%     movies = {'Bang' 'Sophie'}; %
% else
%     movies = {'Bang', 'Sophie', 'Baseline'}; %
% end
%
% movies = {'Bang'};
% bands = {'raw'};
% % bands = {'raw' 'alpha' 'beta' 'theta'};
% % algos ={'CoCA' 'VCoCA'};
% algos ={'CoCA'};
% % algos ={'VCoCA'};
% comp = 1;
% subGroup = [];
% combineCoca;

if strcmp(movies, 'Baseline') && sum(strcmp(Groups, 'Scramble'))
    groups = Groups(1:end-1); %
else
    groups = Groups; %
end
nwin = length(groups)*length(conditions)*length(movies)*length(bands)*length(algos);
rows = 1;

% titles = groups;

labl = zeros(3,nwin);
labl(3,1) = 1;
labl(2,ceil(nwin/2)) = 1 + mod(nwin,2);
labl(1,end) = 1;

h = figure;
% set(h, 'units', 'centimeters', 'pos', [5 2 21 nwin*3.5+2]);
set(h, 'units', 'centimeters', 'pos', [5 5 21 nwin*4]);
ii = 1;
clear pop;
for g = 1:length(groups)
    for c = 1:length(conditions)
        for b = 1:length(bands)
            group = groups{g};
            condition = conditions{c};
            band = bands{b};
            for m = 1:length(movies)
                movie = movies{m};
                for a = 1:length(algos)
                    algo = lower(algos{a});
                    ylimit = [0 ymax];
                    
%                     if strcmp(group,'Joint1')
%                         comp=1;
%                     else
%                         comp=3;
%                     end
                    
                    popCorrelation;
                    
                    
                    %                     axes(h(ii))
                    %                     subpos = [.13 0.98-(ii)*0.80/(nwin) .8 0.75/(nwin)-0.07*3/nwin];
                    %                     subplot(ceil(nwin/rows), rows, ii,'position',subpos);
                    subplot(ceil(nwin/rows), rows, ii)
                    permCorrPlot(pop.(algo).(group).(condition).(movie).(band).corr,...
                        pop.(algo).(group).(condition).(movie).(band).crit(:,1),...
                        'k', 1, ylimit, 14, labl(:,ii));
%                     hold on, plot((1:length(y))/60,y,'r'), hold off
                    t = title(titles{ii});
                    currentPos = get(t, 'position');
                    set(t, 'position',[ currentPos(1), 0.2, 1])
                    %                      title(sprintf('%s %s %s %s %s',algos{a}, group, strcat(condition,' subject'), movie))
                    if ii < nwin
                        set(gca,'XTickLabel','')
                    end
                    set(gca,'YTick',0:0.1:ymax)
                    
                    set(gca,'Visible','on','Box','on')
                    set(gca, 'LooseInset', get(gca,'TightInset'));
                    ii = ii + 1;
                end
            end
        end
    end
end

%
%                     permCorrPlot(pop.(algo).(group).(condition).(movie).(band).corr,...
%                         0.092*ones(length(pop.(algo).(group).(condition).(movie).(band).corr),1),...
%                         'k', 1, ylimit, 14, labl(:,ii));
