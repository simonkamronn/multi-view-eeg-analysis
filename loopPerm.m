pM = [];
pMcoca = [];
for g = 1:length(group)
    idx =  find(not(cellfun('isempty', strfind({cD.Group},group{g}))));
    myGroup = group;
    jointSubGroupCheck
    for c = 1:length(condition)
        for b = 1:length(band)
            for m = 1:length(movie)
                for nc = 1:length(components)
                    
                    fprintf('Starting pm_%s_%s_%s_%s\n',strjoin(myGroup,'_'),strjoin(condition,'_'),strjoin(movie,'_'),strjoin(band,'_'))
                    [ pM ] = permCorrelation( cD, W, movie{m}, condition{c}, band{b}, myGroup{g}, pM, 'VCoCA', idx, nc, psi);
                    
                    fprintf('Starting pmcoca_%s_%s_%s_%s\n',strjoin(myGroup,'_'),strjoin(condition,'_'),strjoin(movie,'_'),strjoin(band,'_'))
                    [ pMcoca ] = permCorrelation( cD, Wcoca, movie{m}, condition{c}, band{b}, myGroup{g}, pMcoca, 'CoCA', idx, nc, []);
                    save(sprintf('pm_%s_%s_%s_%s',strjoin(myGroup,'_'),strjoin(condition,'_'),strjoin(movie,'_'),strjoin(band,'_')), 'pM', 'pMcoca')
                end
            end
        end
    end
end