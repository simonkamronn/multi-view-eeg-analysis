function [ h, groupDelay ] = morletFilter( band, fs )
%Returns the morlet filter in the specified frequency band
%    psiWavelet = ((pi*tP(ii))^(-0.5)).*exp(2*1i*pi*fCenter.*timeVector).*exp(-timeVector.^2/tP(ii));

switch band
    case 'delta'
        a = 0.05;
        fc = 2;
        sigma = 0.1;
        t = -0.33:1/fs:0.33;
    case 'theta'
        a = 0.05;
        fc = 6;
        sigma = 0.12;
        t = -0.5:1/fs:0.5;
    case 'alpha'
        a = 0.05;
        fc = 10;
        sigma = 0.1;
        t = -0.33:1/fs:0.33;
    case 'beta'
        a = 0.2;
        fc = 20;
        sigma = 0.075;
        t = -0.33:1/fs:0.33;
    otherwise
        error('No such band')
end

h = a*exp(2*pi*1i*fc*t).*exp(-(t./(sqrt(2)*sigma)).^2);
groupDelay = (length(h) - 1) / 2;

end

