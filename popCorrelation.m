
if strcmp(condition, 'Intra')
    % VCoCA
    D = size(cp.(algo)(comp).corr_obs.(group).(condition).(movie).(band),2);
    tmp1 = []; tmp2 = [];
    for sub = 1:D
        tmp1 = cat(3, tmp1, cp.(algo)(comp).corr_obs.(group).(condition).(movie).(band)(:,sub));
        tmp2 = cat(3, tmp2,  squeeze(cp.(algo)(comp).crit_corr.(group).(condition).(movie).(band)(:,sub,:)));
    end
    pop.(algo).(group).(condition).(movie).(band).corr = mean(abs(tmp1),3);
    pop.(algo).(group).(condition).(movie).(band).crit = mean(abs(tmp2),3);
    pop.(algo).(group).(condition).(movie).(band).mean = squeeze(mean(abs(tmp1),1));
    pop.(algo).(group).(condition).(movie).(band).var = squeeze(var(abs(tmp1),1));
elseif strcmp(condition, 'Inter')
    D = size(cp.(algo)(comp).corr_obs.(group).(condition).(movie).(band),3);
    C = nchoosek(1:D,2);
    tmp1 = []; tmp2 = [];
    for c1 = 1:D-1
        for c2 = 1:D
            tmp1 = cat(3, tmp1, cp.(algo)(comp).corr_obs.(group).(condition).(movie).(band)(:,c1,c2));
            tmp2 = cat(3, tmp2, squeeze(cp.(algo)(comp).crit_corr.(group).(condition).(movie).(band)(:,c1,c2,:)));
        end
    end
    idx = sum(squeeze(tmp1),1)~=0;
    pop.(algo).(group).(condition).(movie).(band).corr = mean(abs(tmp1(:,:,idx)), 3);
    pop.(algo).(group).(condition).(movie).(band).crit = mean(abs(tmp2(:,:,idx)), 3);
end