function [pM] = permCorrelation( cD, W, movie, condition, band, group, pM, algo, subjectSelection, nC, psi)
% Permutation testing

if nargin < 9, subjectSelection = 1:size(cD,2); end
if nargin < 8, algo = 'VCoCA'; end
if nargin < 7, pM = []; end

fs = 128;
step = fs;
win = 5*fs;
n_perm = 1;
tail = 0;
alpha_level = 0.01;
stat = 'linear';
reports = 0;
N = size(cD(subjectSelection(1)).(movie)(1).(band), 2);
nwin = floor((N-win)/step);

defaultStream=RandStream.getGlobalStream;
for n=1:nwin
    rand(100);
    seed_state(:,n)=defaultStream.State;
end

if strcmp(condition, 'Intra')
    for s = 1:nwin
        mx_corr_sum=zeros(1,n_perm);
        yind = 1+step*(s-1):step*(s-1)+win;
        for tm = subjectSelection
            m = find(subjectSelection == tm);
            if strcmp(algo, 'VCoCA')
                % Backwards model
                W1 = backwardsModel(psi.(group).(condition).(movie).(band)(:,:,1), W.(group).(condition).(movie).(band)(:,nC,1));
                W2 = backwardsModel(psi.(group).(condition).(movie).(band)(:,:,2), W.(group).(condition).(movie).(band)(:,nC,2));
            else
                W1 = W.(group).(condition).(movie).(band)(:,nC);
                W2 = W.(group).(condition).(movie).(band)(:,nC);
            end
            
            Y1 = cD(tm).(movie)(1).(band)(:,yind)' * W1;
            Y2 = cD(tm).(movie)(2).(band)(:,yind)' * W2;

            [   pM(nC).pval.(group).(condition).(movie).(band)(s,m),...
                pM(nC).corr_obs.(group).(condition).(movie).(band)(s,m),...
                pM(nC).crit_corr.(group).(condition).(movie).(band)(s,m,:),...
                ~, ~, mx_corr...
                ] = mult_comp_perm_corr_mod(Y1,Y2, n_perm,tail,alpha_level,...
                stat,reports,seed_state(:,s));
            
            mx_corr_sum = mx_corr_sum + abs(mx_corr);
            
%         fprintf('Correlation of %s.%s.%s.%s between %d done\n',group, condition, movie, band, tm)            
        end
        mx_corr_sum = mx_corr_sum./length(subjectSelection);
        avg_corr = mean(abs( pM(nC).corr_obs.(group).(condition).(movie).(band)(s,:) ));
        pM(nC).pval_avg.(group).(condition).(movie).(band)(s) = ...
            (sum(mx_corr_sum >= avg_corr) + 1) / length(mx_corr_sum);
    end
    
    for tm = subjectSelection
        m = find(subjectSelection == tm);
        
        pM(nC).correlation.(group).(condition).(movie).(band)(m) =... % two-sided test
            (   sum(pM(nC).corr_obs.(group).(condition).(movie).(band)(:,m) < pM(nC).crit_corr.(group).(condition).(movie).(band)(:,m,1))+...
            sum(pM(nC).corr_obs.(group).(condition).(movie).(band)(:,m) > pM(nC).crit_corr.(group).(condition).(movie).(band)(:,m,2)))/length(pM(nC).corr_obs.(group).(condition).(movie).(band)(:,1))*100;
    end
    
    
elseif strcmp(condition, 'Inter')
    if length(subjectSelection) < 2, error('Multiple subjects required'), end
    C = nchoosek(subjectSelection,2);
    for s = 1:nwin
        mx_corr_sum=zeros(1,n_perm);
        avg_corr = 0;
        yind = 1+step*(s-1):step*(s-1)+win;
        
        for c = 1:length(C)
            tc1 = C(c,1);
            tc2 = C(c,2);
            c1 = find(subjectSelection == tc1);
            c2 = find(subjectSelection == tc2);
            if strcmp( algo, 'VCoCA')
                % Backwards model
                W1 = backwardsModel(psi.(group).(condition).(movie).(band)(:,:,c1), W.(group).(condition).(movie).(band)(:,nC,c1));
                W2 = backwardsModel(psi.(group).(condition).(movie).(band)(:,:,c1), W.(group).(condition).(movie).(band)(:,nC,c2));
            else
                W1 = W.(group).(condition).(movie).(band)(:,nC);
                W2 = W.(group).(condition).(movie).(band)(:,nC);
            end
            
            Y1 = cD(tc1).(movie)(1).(band)(:,yind)' * W1;
            Y2 = cD(tc2).(movie)(1).(band)(:,yind)' * W2;
                    
            [   pM(nC).pval.(group).(condition).(movie).(band)(s,c1,c2),...
                corr_obs,...
                pM(nC).crit_corr.(group).(condition).(movie).(band)(s,c1,c2,:),...
                ~, ~, mx_corr]  ...
                = mult_comp_perm_corr_mod(Y1,Y2, n_perm,tail,alpha_level,stat,...
                reports,seed_state(:,s));
            
            pM(nC).corr_obs.(group).(condition).(movie).(band)(s,c1,c2) = corr_obs;
            mx_corr_sum = mx_corr_sum + abs(mx_corr);
            avg_corr = avg_corr + abs(corr_obs);
             
        end
        mx_corr_sum = mx_corr_sum./length(subjectSelection);
        avg_corr = avg_corr/length(subjectSelection);
        pM(nC).pval_avg.(group).(condition).(movie).(band)(s) = ...
            (sum(mx_corr_sum >= avg_corr) + 1) / length(mx_corr_sum);
        
    end
    
    for c = 1:length(C)
        tc1 = C(c,1);
        tc2 = C(c,2);
        c1 = find(subjectSelection == tc1);
        c2 = find(subjectSelection == tc2);
        
        pM(nC).correlation.(group).(condition).(movie).(band)(c1,c2) =... % two-sided test
            (   sum(pM(nC).corr_obs.(group).(condition).(movie).(band)(:,c1,c2) < pM(nC).crit_corr.(group).(condition).(movie).(band)(:,c1,c2,1))+...
            sum(pM(nC).corr_obs.(group).(condition).(movie).(band)(:,c1,c2) > pM(nC).crit_corr.(group).(condition).(movie).(band)(:,c1,c2,2)))/length(pM(nC).corr_obs.(group).(condition).(movie).(band)(:,1))*100;
    end
    
else
    error('Invalid condition')
end

end

function [W] = backwardsModel( R, A)
W = ((A'*pinv(R)*A)\(A'*pinv(R)))';
end

