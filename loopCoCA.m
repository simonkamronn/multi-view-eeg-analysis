for g = 1:length(group)
    idx =  find(not(cellfun('isempty', strfind({cD.Group},group{g}))));
    myGroup = group;
    for c = 1:length(condition)
        for i = 1:length(movie)
            for n = 1:length(band)
                fprintf('Algo: CoCA\nGroup: %s\nCondition: %s\nMovie: %s\nBand: %s\n\n', myGroup{g}, condition{c}, movie{i}, band{n})
                tmp1 = [];
                tmp2 = [];
                if strcmp(condition{c}, 'Inter')
                    C = nchoosek( idx,2);
                    for ii = 1:length(C)
                        tmp1 = cat(2, tmp1, cD(C(ii,1)).(movie{i})(view).(band{n})); % Only first viewings
                        tmp2 = cat(2, tmp2, cD(C(ii,2)).(movie{i})(view).(band{n}));
                    end
                elseif strcmp(condition{c}, 'Intra')
                    for ii = 1:length(idx)
                        tmp1 = cat(2,tmp1, cD(idx(ii)).(movie{i})(1).(band{n}));
                        tmp2 = cat(2,tmp2, cD(idx(ii)).(movie{i})(2).(band{n}));
                    end
                else
                    error('Invalid condition')
                end
                [   Wcoca.(myGroup{g}).(condition{c}).(movie{i}).(band{n}),...
                    Lcoca.(myGroup{g}).(condition{c}).(movie{i}).(band{n})     ] = coca(double(tmp1), double(tmp2));
                
                save(sprintf('CoCA_%s_%s_%s_%s',strjoin(myGroup,'_'),strjoin(condition,'_'),strjoin(movie,'_'),strjoin(band,'_')), 'Wcoca', 'Lcoca', '-mat');
            end
        end
    end
end