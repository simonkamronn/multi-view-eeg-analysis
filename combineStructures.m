function [cp] = combineStructures (pM, cp)
for i = 1:length(pM)
    lvl1 = fieldnames(pM(i));
    for lvl1idx = 1:length(lvl1)        
        lvl2 = fieldnames(pM(i).(lvl1{lvl1idx}));
        for lvl2idx = 1:length(lvl2)
            lvl3 = fieldnames(pM(i).(lvl1{lvl1idx}).(lvl2{lvl2idx}));
            for lvl3idx = 1:length(lvl3)
                lvl4 = fieldnames(pM(i).(lvl1{lvl1idx}).(lvl2{lvl2idx}).(lvl3{lvl3idx}));
                for lvl4idx = 1:length(lvl4)
                    if ~strcmp(lvl1{lvl1idx}, 'mx_corr')
                        cp(i).(lvl1{lvl1idx}).(lvl2{lvl2idx}).(lvl3{lvl3idx}).(lvl4{lvl4idx}) = ...
                            pM(i).(lvl1{lvl1idx}).(lvl2{lvl2idx}).(lvl3{lvl3idx}).(lvl4{lvl4idx});
                    else
                        s1 = size(pM(i).(lvl1{lvl1idx}).(lvl2{lvl2idx}).(lvl3{lvl3idx}).(lvl4{lvl4idx}).raw,2);
                        mx_mean = squeeze(sum(abs(pM(i).(lvl1{lvl1idx}).(lvl2{lvl2idx}).(lvl3{lvl3idx}).(lvl4{lvl4idx}).raw),2))./s1;
                        corr_mean = squeeze(sum(abs(pM(i).corr_obs.(lvl2{lvl2idx}).(lvl3{lvl3idx}).(lvl4{lvl4idx}).raw),2))./s1;
                        
                        if ndims(mx_mean) > 2
                            s2 = size(mx_mean,2);
                            mx_mean = squeeze(sum(mx_mean,2))./(s2/2);
                            corr_mean = squeeze(sum(corr_mean,2))./(s2/2);
                        end
                        cp(i).(lvl1{lvl1idx}).(lvl2{lvl2idx}).(lvl3{lvl3idx}).(lvl4{lvl4idx}) = bsxfun(@plus,sum(bsxfun(@ge,mx_mean,corr_mean),2),1)/length(corr_mean);
                    end
                end
            end
        end
    end
end