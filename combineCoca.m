
if ~exist('cp', 'var')
    cp = struct('coca', [], 'vcoca', []);
    files = dir(fullfile(pwd,''));
    for i = 1:length(files)
    fprintf('Processing %s\n', files(i).name)
        if strcmp(strtok(files(i).name,'_'), 'CoCA')
            load(files(i).name, '-mat')
            cp.coca = combineStructures(Wcoca, cp.coca);
        elseif strcmp(strtok(files(i).name,'_'), 'VCoCA')
            load(files(i).name, '-mat')
            cp.vcoca = combineStructures(W, cp.vcoca);
        elseif strcmp(strtok(files(i).name,'_'), 'pm')
            load(files(i).name, '-mat')
            cp.vcoca = combineStructures(pM, cp.vcoca);
            cp.coca = combineStructures(pMcoca, cp.coca);
        else
            fprintf('No match for %s\n', files(i).name)  
        end
    end
end