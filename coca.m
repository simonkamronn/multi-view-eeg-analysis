function [W,L]=coca(X1,X2)
[D,T] = size(X1); 

% Factor the inputs, and find a full rank set of columns if necessary
% [~,R] = qr(X1,0);
% rankX = sum(abs(diag(R)) > eps(abs(R(1)))*max(D,T));
% if rankX == 0
%     error('Bad data');
% elseif rankX < D
%     warning('Matrix has not full rank');
% end

[Wtemp,Ltemp] = eig((X1*X2'+X2*X1')/T , (X1*X1'+X2*X2')/T);

% Sorterer efter egenværdiernes absolutte størrelser 
Lvec = diag(Ltemp);
[B,I] = sort(abs(Lvec),'descend');
L = diag(Lvec(I));
W=Wtemp(:,I);
end