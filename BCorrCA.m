function [W,Z,Elambda,Epsi,Ealpha,Lbound,V,converged] = BCorrCA(X,opts)
% Bayesiansk CoCA baseret p� bayesiasnk CCA med latent variabel V.
% Skal have alle datas�t samlet i X.
% Indeholder nu udregning af L(q)

timeout =0; % viser repetationer og deres tid

% Finder dimensioner
[D,N,M] = size(X);

% Set defaults
threshold 	= 	1e-6;
nIter 		= 	100;
K 			= 	D;
lSize		=	'Shared';
S0inv		=	1e-3*repmat(eye(D),[1,1,M]);
v0			=	D+1;
converged   =   1;
verbose     =   2;
printIter   =   100; % When to print no. of iterations and time left

% Tjek input
switch nargin
    case 2
        if isfield(opts,'threshold'),	threshold = opts.threshold; end
        if isfield(opts,'nIter'), 		nIter = opts.nIter; end
        if isfield(opts,'K'),           K = opts.K; end
        if isfield(opts,'lSize'), 		lSize = opts.lSize; end
        if isfield(opts,'v'), 			v0 = opts.v; end
        if isfield(opts,'S')
            if ndims(opts.S)==3
                S0inv = opts.S*v0;
            else
                S0inv = repmat(opts.S,[1,1,M])*v0;
            end
        end
        if isfield(opts,'verbose'), 	verbose = opts.verbose; end
        if isfield(opts,'printIter'), 	printIter = opts.printIter; end
    case 1
    otherwise
        error('Forkert antal input')
end


%% Initialiserer (og preallokerer) parametre
% Hyperparametre
a0=1e-3;
b0=a0;
a_alpha = a0 + D/2;
if strcmp(lSize,'K')
    a_lambda = a0 + M*D/2;
elseif strcmp(lSize,'KM')
    a_lambda = a0 + D/2;
else
    a_lambda = a0 + M*D*K/2;
end
v_psi = N + v0;


% Variables
datavar=sum(sum(var(X,0,2)));
alpha = (M*D)./datavar; % fra GFA script (lidt anderledes) ***�ndret
% alpha = 1;
W = zeros(D,K,M);
for m = 1:M;
    W(:,:,m) = randn(D,K)*chol(diag(1./alpha)); % omregner varians og derefter std.
end
Z = zeros(K,N);
Zspec = zeros(K,N,M);
Elambda = 1; %OBS sat lig 1
V = randn(D,K)*chol(diag(1./Elambda));


% Momenter
Epsi = zeros(D,D,M);
for m = 1:M;
    Epsi(:,:,m)=a0*eye(D)/S0inv(:,:,m);
end
Eww = zeros(K,M);
XX = zeros(D,D,M);
for m = 1:M;
    XX(:,:,m)=X(:,:,m)*X(:,:,m)';
end
% Ezz = (N+1)*eye(K);
Sig_wd=zeros(K,K,D,M);
for m=1:M
    for d=1:D
        %         Sig_wd(:,:,d,m) = inv(Epsi(d,d,m)*Ezz + diag(alpha)); % uden lambda
        Sig_wd(:,:,d,m) = b0/a0 *eye(K);
    end
end

Ealpha = alpha*ones(K,1);
Elambda = Elambda*ones(K,M);

%% Updates
%R�kkef�lge: z, W, V, alpha, lambda, Psi, W
tstart=tic;
Iter =1;
run=1;

while run
    
    if verbose>=2 && Iter==round(Iter/printIter)*printIter && Iter>1
        tid=round(toc(tstart)/(Iter-1)*(nIter-Iter+1));
        fprintf('Time spent: %g. Starting rep no.: %g Estimated time left until %g reps: %g seconds. \n'...
            ,toc(tstart),Iter,nIter,tid);
        if Iter > 2
            fprintf('Relative change in lower bound %d  \n', abs(deltaL)/abs(Lbound(Iter-1)));
        end
    end
    
    % Z (Ingen �ndringer ift BCoCA)
    Prec_z = eye(K);
    for m=1:M
        Prec_z = Prec_z + W(:,:,m)'*Epsi(:,:,m)*W(:,:,m);
        for d=1:D
            Prec_z = Prec_z + Epsi(d,d,m)*Sig_wd(:,:,d,m); % �ndres til *diag(diag(Sig_wd(:,:,d,m))) ?
        end
    end
    Sig_z = inv(Prec_z);
    
    for m=1:M
        Zspec(:,:,m) = W(:,:,m)'*Epsi(:,:,m)*X(:,:,m);
    end
    Z = Sig_z*sum(Zspec,3);
    Ezz = N*Sig_z + Z*Z'; % Udregner summen af alle N.
    
    ZX=zeros(K,D,M);
    for m=1:M
        ZX(:,:,m) = Z*X(:,:,m)';
    end
    
    
    % W
    Wold=W;
    for m=1:M
        Tr_sigwk = zeros(K,1); % Trace(Sig_wk)
        for d=1:D
            Sig_wd(:,:,d,m) = inv(Epsi(d,d,m)*Ezz + diag(Elambda(:,m)));
            
            sum1 = Ezz*Wold(1:end~=d,:,m)'*Epsi(d,1:end~=d,m)';
            
            sum2 = ZX(:,:,m)*Epsi(d,:,m)';
            
            W(d,:,m) = Sig_wd(:,:,d,m) * (-sum1 + sum2 + Elambda(:,m).*V(d,:)');
            % Trace(Sig_wk)
            Tr_sigwk = Tr_sigwk + diag(Sig_wd(:,:,d,m));
        end
        
        Eww(:,m) = Tr_sigwk + sum(W(:,:,m).^2)';
    end
    
    
    % V
    sig_v = (sum(Elambda,2) + Ealpha).^(-1);
    lambda_rep = repmat(Elambda,[1,1,D]);
    lambda_shift = shiftdim(lambda_rep,2);
    
    for k=1:K
        V(:,k) = sig_v(k)*sum(lambda_shift(:,k,:).*W(:,k,:),3);
    end
    Tr_sigv = sig_v*D;
    Evv = Tr_sigv + sum(V.^2,1)';
    
    % Alpha
    b_alpha = b0 + Evv/2;
    Ealpha = a_alpha./b_alpha;
    %     alpha_iter(:,Iter)=Ealpha;
    
    
    % lambda
    b_l = repmat(Evv/2,[1,M]) + Eww/2 - permute(sum(W.*repmat(V,[1,1,M]),1),[2,3,1]);
    if strcmp(lSize,'KM')
        b_l2 = b_l;
    elseif strcmp(lSize,'K')
        b_l2 = repmat(sum(b_l,2),[1,M]);
    else
        b_l2 = sum(sum(b_l)) * ones(K,M);
    end
    
    b_lambda = b0 + b_l2;
    Elambda = a_lambda./b_lambda;
    %     lambda(:,:,Iter) = Elambda;
    
    
    % Psi
    %     Epsi_old = Epsi; %til psi_iter
    for m=1:M
        cov_wpw = zeros(D,D);
        for d=1:D
            cov_wpw(d,d) = trace(Sig_wd(:,:,d,m)*Ezz);
        end
        
        %         Epsi(:,:,m) = v_psi*eye(D) / (W(:,:,m)*Ezz*W(:,:,m)' + cov_wpw ...
        %             + XX(:,:,m) - 2*X(:,:,m)*Z'*W(:,:,m)' + S0inv);
        Epsi(:,:,m) = v_psi*eye(D) / (W(:,:,m)*Ezz*W(:,:,m)' + cov_wpw ...
            + XX(:,:,m) - ZX(:,:,m)'*W(:,:,m)' - (ZX(:,:,m)'*W(:,:,m)')' + S0inv(:,:,m));
    end
    %     psi_iter(Iter)=sum(sum(sum(abs(Epsi-Epsi_old))))/(D^2*M);
    
    
    %% L(q) (Lower bound)
    Lq = 0;
    for m=1:M
        Lq = Lq + v_psi*log(det(Epsi(:,:,m)));
        for d=1:D
            Lq = Lq + log(det(Sig_wd(:,:,d,m)));
        end
    end
    Lq = Lq/2;
    
    Lq = Lq - sum(a_alpha*log(b_alpha)) + sum(log(sig_v))*D/2;
    
    Lzz = sum(sum(Z.^2));
    
    if strcmp(lSize,'KM')
        Llambda = -a_lambda*sum(sum(log(b_lambda)));
    elseif strcmp(lSize,'K')
        Llambda = -a_lambda*sum(log(b_lambda(:,1)));
    else
        Llambda = -a_lambda*log(b_lambda(1,1));
    end
    
    
    Lq = Lq + Llambda + N*log(det(Sig_z))/2 - (N*trace(Sig_z) + Lzz)/2;
    
    Lbound(Iter) = Lq;
    
    
    %% Konvergering
    if Iter > 1
        deltaL = Lbound(Iter)-Lbound(Iter-1);
        if abs(deltaL/Lbound(Iter)) < threshold %&& deltaL/abs(Lbound(Iter)) >= 0
            if verbose
                disp(['Converged in ' num2str(Iter) ' iterations'])
            end
            run=0;
        end
    end
    
    if Iter==nIter
        run = 0;
    end
    
    if run==1
        Iter = Iter + 1;
    end
    
end
if Iter == nIter
    if verbose
        disp(['Not converged within ' num2str(nIter) ' iterations'])
        %     disp(['Log likelihood error: ' num2str(abs(oldLogLike - logLike))])
    end
    converged = 0;
    
end

fprintf('Time spent: %g\n',toc(tstart))
end
